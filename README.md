# Leegality Android Helper App#

### Steps to Use Leegality Android APK ###

1. Download Leegality Helper Application from https://play.google.com/store/apps/details?id=com.gspl.leegalityhelper
2. Client Android Application has to submit the eSigning URL to Leegality Helper Application. Please check below snippet :
    <br __>Eg. 
    ```
    Intent intent = new Intent("com.gspl.leegalityhelper.Leegality");
	intent.putExtra("url", "signing-url-here");
	startActivityForResult(intent, REQUEST_CODE);
	//REQUEST_CODE is your constant, which is used to identify particular activity.
    ```

#### Note: It is recommended to start signing process by using startActivityForResult instead of startActivity to receive the response/error. 

### Result: ###

Override method onActivityResult to check the response of signing process.

1. In case of successfully completion of signing process, signing window will be closed automatically and you will get a response with key name 'message' and corresponding 'success message' in the Intent.
2. In case of error in signing process, signing window will be closed automatically and you will get a response with key name 'error' and corresponding 'error message'.


```
@Override
protected void onActivityResult(int requestCode, int resultCode, Intent data){
	//Check requestCode, If it match with the provided REQUEST_CODE then this response is for the signing process.	
	if(requestCode == REQUEST_CODE){
		String error = data.hasExtra("error") ? data.getExtras().getString("error") : null;
		String message = data.hasExtra("message") ? data.getExtras().getString("message") : null;
		if(error != null){		
			System.out.println("Error: " + error);
		}else if(message != null){
			System.out.println("Message: " + message);
		}		
	}
	super.onActivityResult(requestCode, resultCode, data);
}
```